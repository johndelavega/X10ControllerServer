﻿namespace X10ControllerServer
{
    partial class X10Server
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.richTextBoxSendMsg = new System.Windows.Forms.RichTextBox();
            this.richTextBoxReceivedMsg = new System.Windows.Forms.RichTextBox();
            this.textBoxIP = new System.Windows.Forms.TextBox();
            this.textBoxPort = new System.Windows.Forms.TextBox();
            this.buttonStartListen = new System.Windows.Forms.Button();
            this.buttonStopListen = new System.Windows.Forms.Button();
            this.buttonSendMsg = new System.Windows.Forms.Button();
            this.buttonClose = new System.Windows.Forms.Button();
            this.textBoxStatus = new System.Windows.Forms.TextBox();
            this.lblVersion = new System.Windows.Forms.Label();
            this.textBoxReceived = new System.Windows.Forms.TextBox();
            this.buttonClearText = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.labelClientCount = new System.Windows.Forms.Label();
            this.labelONOFF = new System.Windows.Forms.Label();
            this.labelToggle = new System.Windows.Forms.Label();
            this.lblLightBulb1 = new System.Windows.Forms.Label();
            this.lblLightBulb2 = new System.Windows.Forms.Label();
            this.lblLightBulb3 = new System.Windows.Forms.Label();
            this.lblLightBulb4 = new System.Windows.Forms.Label();
            this.lblLightBulb5 = new System.Windows.Forms.Label();
            this.lblIOSVersion = new System.Windows.Forms.Label();
            this.lblDotNetVersion = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.textComPort = new System.Windows.Forms.TextBox();
            this.btnOpenComm = new System.Windows.Forms.Button();
            this.btnCloseComm = new System.Windows.Forms.Button();
            this.buttonON1 = new System.Windows.Forms.Button();
            this.buttonOFF1 = new System.Windows.Forms.Button();
            this.textBoxHouse1 = new System.Windows.Forms.TextBox();
            this.textBoxModule1 = new System.Windows.Forms.TextBox();
            this.textBoxModule2 = new System.Windows.Forms.TextBox();
            this.textBoxHouse2 = new System.Windows.Forms.TextBox();
            this.buttonOFF2 = new System.Windows.Forms.Button();
            this.buttonON2 = new System.Windows.Forms.Button();
            this.textBoxModule3 = new System.Windows.Forms.TextBox();
            this.textBoxHouse3 = new System.Windows.Forms.TextBox();
            this.buttonOFF3 = new System.Windows.Forms.Button();
            this.buttonON3 = new System.Windows.Forms.Button();
            this.textBoxModule4 = new System.Windows.Forms.TextBox();
            this.textBoxHouse4 = new System.Windows.Forms.TextBox();
            this.buttonOFF4 = new System.Windows.Forms.Button();
            this.buttonON4 = new System.Windows.Forms.Button();
            this.textBoxModule5 = new System.Windows.Forms.TextBox();
            this.textBoxHouse5 = new System.Windows.Forms.TextBox();
            this.buttonOFF5 = new System.Windows.Forms.Button();
            this.buttonON5 = new System.Windows.Forms.Button();
            this.textBoxNameLocation1 = new System.Windows.Forms.TextBox();
            this.textBoxNameLocation2 = new System.Windows.Forms.TextBox();
            this.textBoxNameLocation3 = new System.Windows.Forms.TextBox();
            this.textBoxNameLocation5 = new System.Windows.Forms.TextBox();
            this.textBoxNameLocation4 = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(292, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(17, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(286, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(26, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Port";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(506, 113);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Received from client";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(286, 228);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "status";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(291, 113);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Broadcast to client";
            // 
            // richTextBoxSendMsg
            // 
            this.richTextBoxSendMsg.Location = new System.Drawing.Point(290, 129);
            this.richTextBoxSendMsg.Name = "richTextBoxSendMsg";
            this.richTextBoxSendMsg.Size = new System.Drawing.Size(197, 40);
            this.richTextBoxSendMsg.TabIndex = 5;
            this.richTextBoxSendMsg.Text = "";
            // 
            // richTextBoxReceivedMsg
            // 
            this.richTextBoxReceivedMsg.Location = new System.Drawing.Point(509, 130);
            this.richTextBoxReceivedMsg.Name = "richTextBoxReceivedMsg";
            this.richTextBoxReceivedMsg.Size = new System.Drawing.Size(161, 34);
            this.richTextBoxReceivedMsg.TabIndex = 6;
            this.richTextBoxReceivedMsg.Text = "";
            // 
            // textBoxIP
            // 
            this.textBoxIP.Location = new System.Drawing.Point(313, 27);
            this.textBoxIP.Name = "textBoxIP";
            this.textBoxIP.Size = new System.Drawing.Size(100, 20);
            this.textBoxIP.TabIndex = 7;
            this.textBoxIP.Text = "192.168.1.100";
            // 
            // textBoxPort
            // 
            this.textBoxPort.Location = new System.Drawing.Point(315, 58);
            this.textBoxPort.Name = "textBoxPort";
            this.textBoxPort.Size = new System.Drawing.Size(97, 20);
            this.textBoxPort.TabIndex = 8;
            this.textBoxPort.Text = "11001";
            // 
            // buttonStartListen
            // 
            this.buttonStartListen.Location = new System.Drawing.Point(427, 27);
            this.buttonStartListen.Name = "buttonStartListen";
            this.buttonStartListen.Size = new System.Drawing.Size(100, 25);
            this.buttonStartListen.TabIndex = 9;
            this.buttonStartListen.Text = "Start Listen";
            this.buttonStartListen.UseVisualStyleBackColor = true;
            this.buttonStartListen.Click += new System.EventHandler(this.buttonStartListen_Click);
            // 
            // buttonStopListen
            // 
            this.buttonStopListen.Location = new System.Drawing.Point(427, 58);
            this.buttonStopListen.Name = "buttonStopListen";
            this.buttonStopListen.Size = new System.Drawing.Size(100, 25);
            this.buttonStopListen.TabIndex = 10;
            this.buttonStopListen.Text = "Stop Listen";
            this.buttonStopListen.UseVisualStyleBackColor = true;
            this.buttonStopListen.Click += new System.EventHandler(this.buttonStopListen_Click);
            // 
            // buttonSendMsg
            // 
            this.buttonSendMsg.Location = new System.Drawing.Point(291, 176);
            this.buttonSendMsg.Name = "buttonSendMsg";
            this.buttonSendMsg.Size = new System.Drawing.Size(193, 23);
            this.buttonSendMsg.TabIndex = 11;
            this.buttonSendMsg.Text = "Send Message";
            this.buttonSendMsg.UseVisualStyleBackColor = true;
            this.buttonSendMsg.Click += new System.EventHandler(this.buttonSendMsg_Click);
            // 
            // buttonClose
            // 
            this.buttonClose.Location = new System.Drawing.Point(734, 269);
            this.buttonClose.Name = "buttonClose";
            this.buttonClose.Size = new System.Drawing.Size(76, 31);
            this.buttonClose.TabIndex = 12;
            this.buttonClose.Text = "Close";
            this.buttonClose.UseVisualStyleBackColor = true;
            this.buttonClose.Click += new System.EventHandler(this.buttonClose_Click);
            // 
            // textBoxStatus
            // 
            this.textBoxStatus.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxStatus.Location = new System.Drawing.Point(346, 228);
            this.textBoxStatus.Name = "textBoxStatus";
            this.textBoxStatus.Size = new System.Drawing.Size(220, 13);
            this.textBoxStatus.TabIndex = 13;
            this.textBoxStatus.Text = "None";
            // 
            // lblVersion
            // 
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(639, 222);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(43, 13);
            this.lblVersion.TabIndex = 14;
            this.lblVersion.Text = "0.0.001";
            // 
            // textBoxReceived
            // 
            this.textBoxReceived.Location = new System.Drawing.Point(509, 170);
            this.textBoxReceived.Multiline = true;
            this.textBoxReceived.Name = "textBoxReceived";
            this.textBoxReceived.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBoxReceived.Size = new System.Drawing.Size(113, 36);
            this.textBoxReceived.TabIndex = 15;
            // 
            // buttonClearText
            // 
            this.buttonClearText.Location = new System.Drawing.Point(634, 177);
            this.buttonClearText.Name = "buttonClearText";
            this.buttonClearText.Size = new System.Drawing.Size(75, 23);
            this.buttonClearText.TabIndex = 16;
            this.buttonClearText.Text = "Clear Text";
            this.buttonClearText.UseVisualStyleBackColor = true;
            this.buttonClearText.Click += new System.EventHandler(this.buttonClearText_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(289, 245);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(41, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Clients:";
            // 
            // labelClientCount
            // 
            this.labelClientCount.AutoSize = true;
            this.labelClientCount.Location = new System.Drawing.Point(343, 245);
            this.labelClientCount.Name = "labelClientCount";
            this.labelClientCount.Size = new System.Drawing.Size(13, 13);
            this.labelClientCount.TabIndex = 18;
            this.labelClientCount.Text = "0";
            // 
            // labelONOFF
            // 
            this.labelONOFF.AutoSize = true;
            this.labelONOFF.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelONOFF.Location = new System.Drawing.Point(749, 41);
            this.labelONOFF.Name = "labelONOFF";
            this.labelONOFF.Size = new System.Drawing.Size(91, 25);
            this.labelONOFF.TabIndex = 19;
            this.labelONOFF.Text = "ON/OFF";
            // 
            // labelToggle
            // 
            this.labelToggle.AutoSize = true;
            this.labelToggle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelToggle.Location = new System.Drawing.Point(755, 66);
            this.labelToggle.Name = "labelToggle";
            this.labelToggle.Size = new System.Drawing.Size(78, 25);
            this.labelToggle.TabIndex = 20;
            this.labelToggle.Text = "Toggle";
            // 
            // lblLightBulb1
            // 
            this.lblLightBulb1.AutoSize = true;
            this.lblLightBulb1.BackColor = System.Drawing.Color.Silver;
            this.lblLightBulb1.Font = new System.Drawing.Font("Courier New", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLightBulb1.Location = new System.Drawing.Point(32, 25);
            this.lblLightBulb1.Name = "lblLightBulb1";
            this.lblLightBulb1.Size = new System.Drawing.Size(52, 54);
            this.lblLightBulb1.TabIndex = 21;
            this.lblLightBulb1.Text = "1";
            // 
            // lblLightBulb2
            // 
            this.lblLightBulb2.AutoSize = true;
            this.lblLightBulb2.BackColor = System.Drawing.Color.Silver;
            this.lblLightBulb2.Font = new System.Drawing.Font("Courier New", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLightBulb2.Location = new System.Drawing.Point(32, 92);
            this.lblLightBulb2.Name = "lblLightBulb2";
            this.lblLightBulb2.Size = new System.Drawing.Size(52, 54);
            this.lblLightBulb2.TabIndex = 22;
            this.lblLightBulb2.Text = "2";
            // 
            // lblLightBulb3
            // 
            this.lblLightBulb3.AutoSize = true;
            this.lblLightBulb3.BackColor = System.Drawing.Color.Silver;
            this.lblLightBulb3.Font = new System.Drawing.Font("Courier New", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLightBulb3.Location = new System.Drawing.Point(33, 165);
            this.lblLightBulb3.Name = "lblLightBulb3";
            this.lblLightBulb3.Size = new System.Drawing.Size(52, 54);
            this.lblLightBulb3.TabIndex = 23;
            this.lblLightBulb3.Text = "3";
            // 
            // lblLightBulb4
            // 
            this.lblLightBulb4.AutoSize = true;
            this.lblLightBulb4.BackColor = System.Drawing.Color.Silver;
            this.lblLightBulb4.Font = new System.Drawing.Font("Courier New", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLightBulb4.Location = new System.Drawing.Point(33, 235);
            this.lblLightBulb4.Name = "lblLightBulb4";
            this.lblLightBulb4.Size = new System.Drawing.Size(52, 54);
            this.lblLightBulb4.TabIndex = 24;
            this.lblLightBulb4.Text = "4";
            // 
            // lblLightBulb5
            // 
            this.lblLightBulb5.AutoSize = true;
            this.lblLightBulb5.BackColor = System.Drawing.Color.Silver;
            this.lblLightBulb5.Font = new System.Drawing.Font("Courier New", 36F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLightBulb5.Location = new System.Drawing.Point(33, 302);
            this.lblLightBulb5.Name = "lblLightBulb5";
            this.lblLightBulb5.Size = new System.Drawing.Size(52, 54);
            this.lblLightBulb5.TabIndex = 25;
            this.lblLightBulb5.Text = "5";
            // 
            // lblIOSVersion
            // 
            this.lblIOSVersion.AutoSize = true;
            this.lblIOSVersion.Location = new System.Drawing.Point(757, 9);
            this.lblIOSVersion.Name = "lblIOSVersion";
            this.lblIOSVersion.Size = new System.Drawing.Size(72, 13);
            this.lblIOSVersion.TabIndex = 26;
            this.lblIOSVersion.Text = "iOS v.0.0.000";
            // 
            // lblDotNetVersion
            // 
            this.lblDotNetVersion.AutoSize = true;
            this.lblDotNetVersion.Location = new System.Drawing.Point(642, 245);
            this.lblDotNetVersion.Name = "lblDotNetVersion";
            this.lblDotNetVersion.Size = new System.Drawing.Size(52, 13);
            this.lblDotNetVersion.TabIndex = 27;
            this.lblDotNetVersion.Text = "1.0.0.000";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(548, 32);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 102;
            this.label7.Text = "COM Port:";
            // 
            // textComPort
            // 
            this.textComPort.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.textComPort.Location = new System.Drawing.Point(610, 29);
            this.textComPort.Name = "textComPort";
            this.textComPort.Size = new System.Drawing.Size(31, 20);
            this.textComPort.TabIndex = 103;
            this.textComPort.Text = "4";
            // 
            // btnOpenComm
            // 
            this.btnOpenComm.Location = new System.Drawing.Point(657, 27);
            this.btnOpenComm.Name = "btnOpenComm";
            this.btnOpenComm.Size = new System.Drawing.Size(75, 23);
            this.btnOpenComm.TabIndex = 101;
            this.btnOpenComm.Text = "Open";
            this.btnOpenComm.UseVisualStyleBackColor = true;
            this.btnOpenComm.Click += new System.EventHandler(this.btnOpenComm_Click);
            // 
            // btnCloseComm
            // 
            this.btnCloseComm.Location = new System.Drawing.Point(657, 56);
            this.btnCloseComm.Name = "btnCloseComm";
            this.btnCloseComm.Size = new System.Drawing.Size(75, 23);
            this.btnCloseComm.TabIndex = 104;
            this.btnCloseComm.Text = "Close";
            this.btnCloseComm.UseVisualStyleBackColor = true;
            this.btnCloseComm.Click += new System.EventHandler(this.btnCloseComm_Click);
            // 
            // buttonON1
            // 
            this.buttonON1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonON1.Location = new System.Drawing.Point(90, 61);
            this.buttonON1.Name = "buttonON1";
            this.buttonON1.Size = new System.Drawing.Size(23, 19);
            this.buttonON1.TabIndex = 105;
            this.buttonON1.Text = "on";
            this.buttonON1.UseVisualStyleBackColor = true;
            this.buttonON1.Click += new System.EventHandler(this.buttonON1_Click);
            // 
            // buttonOFF1
            // 
            this.buttonOFF1.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOFF1.Location = new System.Drawing.Point(116, 61);
            this.buttonOFF1.Name = "buttonOFF1";
            this.buttonOFF1.Size = new System.Drawing.Size(25, 19);
            this.buttonOFF1.TabIndex = 106;
            this.buttonOFF1.Text = "off";
            this.buttonOFF1.UseVisualStyleBackColor = true;
            this.buttonOFF1.Click += new System.EventHandler(this.buttonOFF1_Click);
            // 
            // textBoxHouse1
            // 
            this.textBoxHouse1.Location = new System.Drawing.Point(90, 28);
            this.textBoxHouse1.Name = "textBoxHouse1";
            this.textBoxHouse1.Size = new System.Drawing.Size(14, 20);
            this.textBoxHouse1.TabIndex = 107;
            this.textBoxHouse1.Text = "A";
            this.textBoxHouse1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxModule1
            // 
            this.textBoxModule1.Location = new System.Drawing.Point(107, 28);
            this.textBoxModule1.Name = "textBoxModule1";
            this.textBoxModule1.Size = new System.Drawing.Size(20, 20);
            this.textBoxModule1.TabIndex = 108;
            this.textBoxModule1.Text = "1";
            this.textBoxModule1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxModule2
            // 
            this.textBoxModule2.Location = new System.Drawing.Point(107, 97);
            this.textBoxModule2.Name = "textBoxModule2";
            this.textBoxModule2.Size = new System.Drawing.Size(20, 20);
            this.textBoxModule2.TabIndex = 112;
            this.textBoxModule2.Text = "5";
            this.textBoxModule2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxHouse2
            // 
            this.textBoxHouse2.Location = new System.Drawing.Point(90, 97);
            this.textBoxHouse2.Name = "textBoxHouse2";
            this.textBoxHouse2.Size = new System.Drawing.Size(14, 20);
            this.textBoxHouse2.TabIndex = 111;
            this.textBoxHouse2.Text = "B";
            this.textBoxHouse2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonOFF2
            // 
            this.buttonOFF2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOFF2.Location = new System.Drawing.Point(116, 126);
            this.buttonOFF2.Name = "buttonOFF2";
            this.buttonOFF2.Size = new System.Drawing.Size(25, 19);
            this.buttonOFF2.TabIndex = 110;
            this.buttonOFF2.Text = "off";
            this.buttonOFF2.UseVisualStyleBackColor = true;
            this.buttonOFF2.Click += new System.EventHandler(this.buttonOFF2_Click);
            // 
            // buttonON2
            // 
            this.buttonON2.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonON2.Location = new System.Drawing.Point(88, 127);
            this.buttonON2.Name = "buttonON2";
            this.buttonON2.Size = new System.Drawing.Size(23, 19);
            this.buttonON2.TabIndex = 109;
            this.buttonON2.Text = "on";
            this.buttonON2.UseVisualStyleBackColor = true;
            this.buttonON2.Click += new System.EventHandler(this.buttonON2_Click);
            // 
            // textBoxModule3
            // 
            this.textBoxModule3.Location = new System.Drawing.Point(109, 167);
            this.textBoxModule3.Name = "textBoxModule3";
            this.textBoxModule3.Size = new System.Drawing.Size(20, 20);
            this.textBoxModule3.TabIndex = 116;
            this.textBoxModule3.Text = "8";
            this.textBoxModule3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxHouse3
            // 
            this.textBoxHouse3.Location = new System.Drawing.Point(92, 167);
            this.textBoxHouse3.Name = "textBoxHouse3";
            this.textBoxHouse3.Size = new System.Drawing.Size(14, 20);
            this.textBoxHouse3.TabIndex = 115;
            this.textBoxHouse3.Text = "C";
            this.textBoxHouse3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonOFF3
            // 
            this.buttonOFF3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOFF3.Location = new System.Drawing.Point(116, 199);
            this.buttonOFF3.Name = "buttonOFF3";
            this.buttonOFF3.Size = new System.Drawing.Size(25, 19);
            this.buttonOFF3.TabIndex = 114;
            this.buttonOFF3.Text = "off";
            this.buttonOFF3.UseVisualStyleBackColor = true;
            this.buttonOFF3.Click += new System.EventHandler(this.buttonOFF3_Click);
            // 
            // buttonON3
            // 
            this.buttonON3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonON3.Location = new System.Drawing.Point(90, 199);
            this.buttonON3.Name = "buttonON3";
            this.buttonON3.Size = new System.Drawing.Size(23, 19);
            this.buttonON3.TabIndex = 113;
            this.buttonON3.Text = "on";
            this.buttonON3.UseVisualStyleBackColor = true;
            this.buttonON3.Click += new System.EventHandler(this.buttonON3_Click);
            // 
            // textBoxModule4
            // 
            this.textBoxModule4.Location = new System.Drawing.Point(110, 237);
            this.textBoxModule4.Name = "textBoxModule4";
            this.textBoxModule4.Size = new System.Drawing.Size(20, 20);
            this.textBoxModule4.TabIndex = 120;
            this.textBoxModule4.Text = "13";
            this.textBoxModule4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxHouse4
            // 
            this.textBoxHouse4.Location = new System.Drawing.Point(93, 237);
            this.textBoxHouse4.Name = "textBoxHouse4";
            this.textBoxHouse4.Size = new System.Drawing.Size(14, 20);
            this.textBoxHouse4.TabIndex = 119;
            this.textBoxHouse4.Text = "D";
            this.textBoxHouse4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonOFF4
            // 
            this.buttonOFF4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOFF4.Location = new System.Drawing.Point(118, 269);
            this.buttonOFF4.Name = "buttonOFF4";
            this.buttonOFF4.Size = new System.Drawing.Size(25, 19);
            this.buttonOFF4.TabIndex = 118;
            this.buttonOFF4.Text = "off";
            this.buttonOFF4.UseVisualStyleBackColor = true;
            this.buttonOFF4.Click += new System.EventHandler(this.buttonOFF4_Click);
            // 
            // buttonON4
            // 
            this.buttonON4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonON4.Location = new System.Drawing.Point(92, 269);
            this.buttonON4.Name = "buttonON4";
            this.buttonON4.Size = new System.Drawing.Size(23, 19);
            this.buttonON4.TabIndex = 117;
            this.buttonON4.Text = "on";
            this.buttonON4.UseVisualStyleBackColor = true;
            this.buttonON4.Click += new System.EventHandler(this.buttonON4_Click);
            // 
            // textBoxModule5
            // 
            this.textBoxModule5.Location = new System.Drawing.Point(110, 303);
            this.textBoxModule5.Name = "textBoxModule5";
            this.textBoxModule5.Size = new System.Drawing.Size(20, 20);
            this.textBoxModule5.TabIndex = 124;
            this.textBoxModule5.Text = "16";
            this.textBoxModule5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxHouse5
            // 
            this.textBoxHouse5.Location = new System.Drawing.Point(93, 303);
            this.textBoxHouse5.Name = "textBoxHouse5";
            this.textBoxHouse5.Size = new System.Drawing.Size(14, 20);
            this.textBoxHouse5.TabIndex = 123;
            this.textBoxHouse5.Text = "E";
            this.textBoxHouse5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonOFF5
            // 
            this.buttonOFF5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonOFF5.Location = new System.Drawing.Point(121, 335);
            this.buttonOFF5.Name = "buttonOFF5";
            this.buttonOFF5.Size = new System.Drawing.Size(25, 19);
            this.buttonOFF5.TabIndex = 122;
            this.buttonOFF5.Text = "off";
            this.buttonOFF5.UseVisualStyleBackColor = true;
            this.buttonOFF5.Click += new System.EventHandler(this.buttonOFF5_Click);
            // 
            // buttonON5
            // 
            this.buttonON5.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonON5.Location = new System.Drawing.Point(95, 335);
            this.buttonON5.Name = "buttonON5";
            this.buttonON5.Size = new System.Drawing.Size(23, 19);
            this.buttonON5.TabIndex = 121;
            this.buttonON5.Text = "on";
            this.buttonON5.UseVisualStyleBackColor = true;
            this.buttonON5.Click += new System.EventHandler(this.buttonON5_Click);
            // 
            // textBoxNameLocation1
            // 
            this.textBoxNameLocation1.Location = new System.Drawing.Point(137, 29);
            this.textBoxNameLocation1.Name = "textBoxNameLocation1";
            this.textBoxNameLocation1.Size = new System.Drawing.Size(100, 20);
            this.textBoxNameLocation1.TabIndex = 125;
            this.textBoxNameLocation1.Text = "Living Room";
            // 
            // textBoxNameLocation2
            // 
            this.textBoxNameLocation2.Location = new System.Drawing.Point(137, 96);
            this.textBoxNameLocation2.Name = "textBoxNameLocation2";
            this.textBoxNameLocation2.Size = new System.Drawing.Size(100, 20);
            this.textBoxNameLocation2.TabIndex = 126;
            this.textBoxNameLocation2.Text = "Master Bedroom";
            // 
            // textBoxNameLocation3
            // 
            this.textBoxNameLocation3.Location = new System.Drawing.Point(140, 168);
            this.textBoxNameLocation3.Name = "textBoxNameLocation3";
            this.textBoxNameLocation3.Size = new System.Drawing.Size(100, 20);
            this.textBoxNameLocation3.TabIndex = 127;
            this.textBoxNameLocation3.Text = "Master Bedroom";
            // 
            // textBoxNameLocation5
            // 
            this.textBoxNameLocation5.Location = new System.Drawing.Point(139, 303);
            this.textBoxNameLocation5.Name = "textBoxNameLocation5";
            this.textBoxNameLocation5.Size = new System.Drawing.Size(100, 20);
            this.textBoxNameLocation5.TabIndex = 129;
            this.textBoxNameLocation5.Text = "Bedroom 2";
            // 
            // textBoxNameLocation4
            // 
            this.textBoxNameLocation4.Location = new System.Drawing.Point(139, 238);
            this.textBoxNameLocation4.Name = "textBoxNameLocation4";
            this.textBoxNameLocation4.Size = new System.Drawing.Size(100, 20);
            this.textBoxNameLocation4.TabIndex = 128;
            this.textBoxNameLocation4.Text = "Master Bedroom";
            // 
            // X10Server
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(858, 385);
            this.Controls.Add(this.textBoxNameLocation5);
            this.Controls.Add(this.textBoxNameLocation4);
            this.Controls.Add(this.textBoxNameLocation3);
            this.Controls.Add(this.textBoxNameLocation2);
            this.Controls.Add(this.textBoxNameLocation1);
            this.Controls.Add(this.textBoxModule5);
            this.Controls.Add(this.textBoxHouse5);
            this.Controls.Add(this.buttonOFF5);
            this.Controls.Add(this.buttonON5);
            this.Controls.Add(this.textBoxModule4);
            this.Controls.Add(this.textBoxHouse4);
            this.Controls.Add(this.buttonOFF4);
            this.Controls.Add(this.buttonON4);
            this.Controls.Add(this.textBoxModule3);
            this.Controls.Add(this.textBoxHouse3);
            this.Controls.Add(this.buttonOFF3);
            this.Controls.Add(this.buttonON3);
            this.Controls.Add(this.textBoxModule2);
            this.Controls.Add(this.textBoxHouse2);
            this.Controls.Add(this.buttonOFF2);
            this.Controls.Add(this.buttonON2);
            this.Controls.Add(this.textBoxModule1);
            this.Controls.Add(this.textBoxHouse1);
            this.Controls.Add(this.buttonOFF1);
            this.Controls.Add(this.buttonON1);
            this.Controls.Add(this.btnCloseComm);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textComPort);
            this.Controls.Add(this.btnOpenComm);
            this.Controls.Add(this.lblDotNetVersion);
            this.Controls.Add(this.lblIOSVersion);
            this.Controls.Add(this.lblLightBulb5);
            this.Controls.Add(this.lblLightBulb4);
            this.Controls.Add(this.lblLightBulb3);
            this.Controls.Add(this.lblLightBulb2);
            this.Controls.Add(this.lblLightBulb1);
            this.Controls.Add(this.labelToggle);
            this.Controls.Add(this.labelONOFF);
            this.Controls.Add(this.labelClientCount);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.buttonClearText);
            this.Controls.Add(this.textBoxReceived);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.textBoxStatus);
            this.Controls.Add(this.buttonClose);
            this.Controls.Add(this.buttonSendMsg);
            this.Controls.Add(this.buttonStopListen);
            this.Controls.Add(this.buttonStartListen);
            this.Controls.Add(this.textBoxPort);
            this.Controls.Add(this.textBoxIP);
            this.Controls.Add(this.richTextBoxReceivedMsg);
            this.Controls.Add(this.richTextBoxSendMsg);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Location = new System.Drawing.Point(700, 100);
            this.Name = "X10Server";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "X10 Controller Server";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.RichTextBox richTextBoxSendMsg;
        private System.Windows.Forms.RichTextBox richTextBoxReceivedMsg;
        private System.Windows.Forms.TextBox textBoxIP;
        private System.Windows.Forms.TextBox textBoxPort;
        private System.Windows.Forms.Button buttonStartListen;
        private System.Windows.Forms.Button buttonStopListen;
        private System.Windows.Forms.Button buttonSendMsg;
        private System.Windows.Forms.Button buttonClose;
        private System.Windows.Forms.TextBox textBoxStatus;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.TextBox textBoxReceived;
        private System.Windows.Forms.Button buttonClearText;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label labelClientCount;
        private System.Windows.Forms.Label labelONOFF;
        private System.Windows.Forms.Label labelToggle;
        private System.Windows.Forms.Label lblLightBulb1;
        private System.Windows.Forms.Label lblLightBulb2;
        private System.Windows.Forms.Label lblLightBulb3;
        private System.Windows.Forms.Label lblLightBulb4;
        private System.Windows.Forms.Label lblLightBulb5;
        private System.Windows.Forms.Label lblIOSVersion;
        private System.Windows.Forms.Label lblDotNetVersion;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox textComPort;
        private System.Windows.Forms.Button btnOpenComm;
        private System.Windows.Forms.Button btnCloseComm;
        private System.Windows.Forms.Button buttonON1;
        private System.Windows.Forms.Button buttonOFF1;
        private System.Windows.Forms.TextBox textBoxHouse1;
        private System.Windows.Forms.TextBox textBoxModule1;
        private System.Windows.Forms.TextBox textBoxModule2;
        private System.Windows.Forms.TextBox textBoxHouse2;
        private System.Windows.Forms.Button buttonOFF2;
        private System.Windows.Forms.Button buttonON2;
        private System.Windows.Forms.TextBox textBoxModule3;
        private System.Windows.Forms.TextBox textBoxHouse3;
        private System.Windows.Forms.Button buttonOFF3;
        private System.Windows.Forms.Button buttonON3;
        private System.Windows.Forms.TextBox textBoxModule4;
        private System.Windows.Forms.TextBox textBoxHouse4;
        private System.Windows.Forms.Button buttonOFF4;
        private System.Windows.Forms.Button buttonON4;
        private System.Windows.Forms.TextBox textBoxModule5;
        private System.Windows.Forms.TextBox textBoxHouse5;
        private System.Windows.Forms.Button buttonOFF5;
        private System.Windows.Forms.Button buttonON5;
        private System.Windows.Forms.TextBox textBoxNameLocation1;
        private System.Windows.Forms.TextBox textBoxNameLocation2;
        private System.Windows.Forms.TextBox textBoxNameLocation3;
        private System.Windows.Forms.TextBox textBoxNameLocation5;
        private System.Windows.Forms.TextBox textBoxNameLocation4;
    }
}

