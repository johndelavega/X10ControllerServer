﻿
// Visual Studio 2008 | VC9

// 00:00 AM 00/00/0000 | build 0001.1

//  5:18 PM 11/23/2012 | build 0001 | renamed proeject to X10Controller from GC_SocketServer
// 00:00 AM 00/00/0000


using System;
//using System.Collections.Generic;
//using System.ComponentModel;
//using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
using System.Windows.Forms;

using System.Net;
using System.Net.Sockets;

using System.IO.Ports;

using System.Reflection;

namespace X10ControllerServer
{
    public partial class X10Server : Form
    {
        private SerialPort m_comm = new SerialPort("COM4", 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);

        const int MAX_CLIENTS = 10;

        public AsyncCallback pfnWorkerCallBack;
        private Socket m_mainSocket;
        private Socket[] m_workerSocket = new Socket[10];
        private int m_clientCount = 0;


        public X10Server()
        {
            InitializeComponent();

            openComm();

//version
            Assembly asm = Assembly.GetExecutingAssembly();
            lblVersion.Text = "v" + asm.GetName().Version.ToString();
            lblDotNetVersion.Text = ".NET " + asm.ImageRuntimeVersion;

            // this information is taken from AssembyInfo.cs
            // we can use this as the internal build, 
            // comment out in formal build
            //lblVersion.Text = "0.0.0001.1"; // 0001a

            Version v = Environment.Version;
            lblDotNetVersion.Text = ".NET " +v.ToString();
            //label2.Text = v.ToString();
            //MessageBox.Show( v.ToString());
//version


            // Display the local IP address on the GUI
            textBoxIP.Text = GetIP();

//this.formstartposition = 500;

StartListinng();

        }


        private void buttonStartListen_Click(object sender, EventArgs e)
        {

StartListinng();

#if FALSE 
           try
            {
                // Check the port value
                if (textBoxPort.Text == "")
                {
                    MessageBox.Show("Please enter a Port Number");
                    return;
                }
                string portStr = textBoxPort.Text;
                int port = System.Convert.ToInt32(portStr);
                // Create the listening socket...
                m_mainSocket = new Socket(AddressFamily.InterNetwork,
                                          SocketType.Stream,
                                          ProtocolType.Tcp);
                IPEndPoint ipLocal = new IPEndPoint(IPAddress.Any, port);
                // Bind to local IP Address...
                m_mainSocket.Bind(ipLocal);
                // Start listening...
                m_mainSocket.Listen(100);
                // Create the call back for any client connections...
                m_mainSocket.BeginAccept(new AsyncCallback(OnClientConnect), null);

                UpdateControls(true);
textBoxStatus.Text = "Listening...";
Console.WriteLine("Listening...");
            }
            catch (SocketException se)
            {
                MessageBox.Show(se.Message);
            }
#endif

        } // buttonStartListen_Click



private void StartListinng()
{

    try
    {
        // Check the port value
        if (textBoxPort.Text == "")
        {
            MessageBox.Show("Please enter a Port Number");
            return;
        }
        string portStr = textBoxPort.Text;
        int port = System.Convert.ToInt32(portStr);
        // Create the listening socket...
        m_mainSocket = new Socket(AddressFamily.InterNetwork,
                                  SocketType.Stream,
                                  ProtocolType.Tcp);
        IPEndPoint ipLocal = new IPEndPoint(IPAddress.Any, port);
        // Bind to local IP Address...
        m_mainSocket.Bind(ipLocal);
        // Start listening...
        m_mainSocket.Listen(100);
        // Create the call back for any client connections...
        m_mainSocket.BeginAccept(new AsyncCallback(OnClientConnect), null);

        UpdateControls(true);
        textBoxStatus.Text = "Listening...";
        Console.WriteLine("Listening...");
    }
    catch (SocketException se)
    {
        MessageBox.Show(se.Message);
    }

} // void StartListinng()





        private void UpdateControls(bool listening)
        {
            buttonStartListen.Enabled = !listening;
            buttonStopListen.Enabled = listening;
        }

        // This is the call back function, which will be invoked when a client is connected
        public void OnClientConnect(IAsyncResult asyn)
        {
            try
            {
                // Here we complete/end the BeginAccept() asynchronous call
                // by calling EndAccept() - which returns the reference to
                // a new Socket object
                m_workerSocket[m_clientCount] = m_mainSocket.EndAccept(asyn);
                // Let the worker Socket do the further processing for the 
                // just connected client
                WaitForData(m_workerSocket[m_clientCount]);
                // Now increment the client count
                ++m_clientCount;
                // Display this client connection as a status message on the GUI	
                String str = String.Format("\nClient # {0} connected", m_clientCount);
 Console.WriteLine(str);
//egd1
//               textBoxMsg.Text = str;


 ///...blah blah updating files
 ///string newText = "abc"; // running on worker thread
 this.Invoke((MethodInvoker)delegate
 {
     textBoxStatus.Text = str; // runs on UI thread
labelClientCount.Text = m_clientCount.ToString();
 });
 ///...blah blah more updating files


                // Since the main Socket is now free, it can go back and wait for
                // other clients who are attempting to connect
                m_mainSocket.BeginAccept(new AsyncCallback(OnClientConnect), null);
            }
            catch (ObjectDisposedException)
            {
                System.Diagnostics.Debugger.Log(0, "1", "\n OnClientConnection: Socket has been closed\n");
            }
            catch (SocketException se)
            {
                MessageBox.Show(se.Message);
            }

        }


        public class SocketPacket
        {
            public System.Net.Sockets.Socket m_currentSocket;
            public byte[] dataBuffer = new byte[1024];
        }



        // Start waiting for data from the client
        public void WaitForData(System.Net.Sockets.Socket soc)
        {
            try
            {
                if (pfnWorkerCallBack == null)
                {
                    // Specify the call back function which is to be 
                    // invoked when there is any write activity by the 
                    // connected client
                    pfnWorkerCallBack = new AsyncCallback(OnDataReceived);
                }
                SocketPacket theSocPkt = new SocketPacket();
                theSocPkt.m_currentSocket = soc;
                // Start receiving any data written by the connected client
                // asynchronously
                soc.BeginReceive(theSocPkt.dataBuffer, 0,
                                   theSocPkt.dataBuffer.Length,
                                   SocketFlags.None,
                                   pfnWorkerCallBack,
                                   theSocPkt);
            }
            catch (SocketException se)
            {
                MessageBox.Show(se.Message);
            }

        }

        private static long OnDataReceived_Zero_Count = 0;

        //--------------------------------------------------------------------------

        // NOTE: egd1: 1 char is being read at a time !!!

        // This the call back function which will be invoked when the socket
        // detects any client writing of data on the stream
        public void OnDataReceived(IAsyncResult asyn)
        {
            try
            {
                SocketPacket socketData = (SocketPacket)asyn.AsyncState;

                bool b1 = asyn.CompletedSynchronously;
                bool b2 = asyn.IsCompleted;

                bool b = socketData.m_currentSocket.Connected;

                if (b)
                    Console.WriteLine("\n OnDataReceived(): Socket is Connected! :)\n");
                else
                    Console.WriteLine("\n OnDataReceived(): Socket is NOT Connected! :(\n");


                int iRx = 0;

                // Complete the BeginReceive() asynchronous call by EndReceive() method
                // which will return the number of characters written to the stream 
                // by the client

                iRx = socketData.m_currentSocket.EndReceive(asyn);

                if (iRx == 0)
                {
                OnDataReceived_Zero_Count++;
                Console.WriteLine("OnDataReceived_Zero_Count = {0}", OnDataReceived_Zero_Count);
                }

                if ((iRx == 0) && (OnDataReceived_Zero_Count > 5))
                {
                    OnDataReceived_Zero_Count = 0;

                    Console.WriteLine("OnDataReceived: zero data received, OnDataReceived_Zero_Count = {0}", OnDataReceived_Zero_Count);

                    if (m_clientCount > 0)
                        m_clientCount--;

                    this.Invoke((MethodInvoker)delegate
                    { // runs on UI thread
                      textBoxStatus.Text = "Zero data, Listening...";
                      labelClientCount.Text = m_clientCount.ToString();
                    });

                    return;

                } // if ((iRx == 0) && ...


        System.String szData = "Null";

        if (iRx != 0)
        {
            char[] chars = new char[iRx + 1];
            System.Text.Decoder d = System.Text.Encoding.UTF8.GetDecoder();
            int charLen = d.GetChars(socketData.dataBuffer,
                                     0, iRx, chars, 0);
            szData = new System.String(chars);

            //}
            Console.WriteLine("OnDataReceived try catch : iRx != 0");

            //egd1
            Console.WriteLine(szData + "\n\n");
            //                richTextBoxReceivedMsg.AppendText(szData);


            // this area is running on Socket thread

            this.Invoke((MethodInvoker)delegate
            {
                // runs on UI thread

                //szData = szData + "  test";

                //textBoxReceived.AppendText(szData);
                textBoxReceived.Text = textBoxReceived.Text + szData;
                //richTextBoxReceivedMsg.Text = "szData";

                richTextBoxReceivedMsg.AppendText(szData);

                if (szData.Equals("ON\0"))
                {
                    labelONOFF.Text = "ON";
                    labelONOFF.BackColor = Color.Green;
                    labelONOFF.ForeColor = Color.White;
                }
                else if (szData.Equals("OFF\0"))
                {
                    labelONOFF.Text = "OFF";
                    labelONOFF.BackColor = Color.Red;
                    labelONOFF.ForeColor = Color.White;
                }

                if (szData.Equals("Toggle\0"))
                {
                  //  labelONOFF.BackColor = labelONOFF.BackColor == Color.Green ? Color.Red : Color.Green;

                    if (labelToggle.BackColor == Color.Green)
                    {
                        labelToggle.BackColor = Color.Red;
                        labelToggle.ForeColor = Color.White;
                    }
                    else
                    {
                        labelToggle.BackColor = Color.Green;
                        labelToggle.ForeColor = Color.Black;
                    }
                }

                if (szData.Equals("ON1\0"))
                    lblLightBulb1.BackColor = Color.Yellow;
                else if (szData.Equals("OFF1\0"))
                    lblLightBulb1.BackColor = Color.Silver;


                if (szData.Equals("ON2\0"))
                {
                   // labelONOFF.Text = "ON";
                    lblLightBulb2.BackColor = Color.Yellow;
                    //labelONOFF.ForeColor = Color.White;
                }
                else if (szData.Equals("OFF2\0"))
                {
                  //  labelONOFF.Text = "OFF";
                    lblLightBulb2.BackColor = Color.Silver;
                    //labelONOFF.ForeColor = Color.White;
                }

                if (szData.Equals("ON3\0"))
                    lblLightBulb3.BackColor = Color.Yellow;
                else if (szData.Equals("OFF3\0"))
                    lblLightBulb3.BackColor = Color.Silver;


                if (szData.Equals("ON4\0")) // KITCHEN
                {
                    // labelONOFF.Text = "ON";
                    lblLightBulb4.BackColor = Color.Yellow;
                    //labelONOFF.ForeColor = Color.White;

                    DebugPrint("Send bits ON E7");
                    SendCommand("1000000001001000"); // ON E7
                }
                else if (szData.Equals("OFF4\0"))
                {
                    //  labelONOFF.Text = "OFF";
                    lblLightBulb4.BackColor = Color.Silver;
                    //labelONOFF.ForeColor = Color.White;

                    DebugPrint("Send bits OFF E7");
                    SendCommand("1000000001101000");
                }

                if (szData.Equals("ON5\0")) // LIVING ROOM
                {
                    lblLightBulb5.BackColor = Color.Yellow;

                    DebugPrint("E8 ON");
                    SendCommand("1000000001011000");
                }
                else if (szData.Equals("OFF5\0"))
                {
                    lblLightBulb5.BackColor = Color.Silver;

                    DebugPrint("E8 OFF");
                    SendCommand("1000000001111000");

                }

                // iOS v.

                 bool bVer = szData.StartsWith("iOS v.");

                if (bVer)
                lblIOSVersion.Text = szData;



                //szData.lef
                //if (szData.Equals("ON5\0"))


                }); // this.Invoke((MethodInvoker)delegate

        } // if (iRx != 0)


        // Continue the waiting for data on the Socket
        WaitForData(socketData.m_currentSocket);


#if FALSE

// egd1: THIS GOES INTO INIFINITE LOOP, iRx == 0 is the stopping condition and disconnects

if (iRx != 0)
                // Continue the waiting for data on the Socket
                WaitForData(socketData.m_currentSocket);
else
{



    CloseSockets();

    this.Invoke((MethodInvoker)delegate
    {
        // runs on UI thread

        UpdateControls(false);
        textBoxStatus.Text = "None";
        richTextBoxReceivedMsg.Clear();
        textBoxReceived.Clear();

    });


            //CloseSockets();
            //UpdateControls(false);
            //textBoxStatus.Text = "None";
}
#endif


            }
            catch (ObjectDisposedException)
            {
                System.Diagnostics.Debugger.Log(0, "1", "\nOnDataReceived: Socket has been closed\n");
            }
            catch (SocketException se)
            {

                // connection closed remotely?

                Console.WriteLine("exception message:   " + se.Message);

                //                CloseSockets();
                if (m_clientCount > 0)
                m_clientCount--;

                 this.Invoke((MethodInvoker)delegate
                 {
                // runs on UI thread
                     labelClientCount.Text = m_clientCount.ToString();
                //    UpdateControls(false);
                    textBoxStatus.Text = "Closed Remotely, Listening...";
                    //richTextBoxReceivedMsg.Clear();
                    //textBoxReceived.Clear();

                });

                Console.WriteLine("Connection closed remotely");
                //                MessageBox.Show(se.Message);
            
            }// catch

        }// OnDataReceived


        //--------------------------------------------------------------------------


        private void buttonSendMsg_Click(object sender, EventArgs e)
        {
            try
            {
                Object objData = richTextBoxSendMsg.Text;
                byte[] byData = System.Text.Encoding.ASCII.GetBytes(objData.ToString());
                for (int i = 0; i < m_clientCount; i++)
                {
                    if (m_workerSocket[i] != null)
                    {
                        if (m_workerSocket[i].Connected)
                        {
                            m_workerSocket[i].Send(byData);
                        }
                    }
                }

            }
            catch (SocketException se)
            {
                MessageBox.Show(se.Message);
            }
        }

        private void buttonStopListen_Click(object sender, EventArgs e)
        {
            CloseSockets();
            UpdateControls(false);
            textBoxStatus.Text = "None";
        }



        String GetIP()
        {
            String strHostName = Dns.GetHostName();

            // Find host by name
            IPHostEntry iphostentry = Dns.GetHostByName(strHostName);

            // Grab the first IP addresses
            String IPStr = "";
            foreach (IPAddress ipaddress in iphostentry.AddressList)
            {
                IPStr = ipaddress.ToString();
                return IPStr;
            }
            return IPStr;
        }

        private void buttonClose_Click(object sender, EventArgs e)
        {
            CloseSockets();
            Close();
        }


        void CloseSockets()
        {
Console.WriteLine("CloseSockets()");
            if (m_mainSocket != null)
            {
                m_mainSocket.Close();
            }
            for (int i = 0; i < m_clientCount; i++)
            {
                if (m_workerSocket[i] != null)
                {
                    m_workerSocket[i].Close();
                    m_workerSocket[i] = null;
                }
            }
// labelClientCount.Text = m_clientCount.ToString();
if (m_clientCount > 0)
m_clientCount--;
        }

        private void buttonClearText_Click(object sender, EventArgs e)
        {
            richTextBoxReceivedMsg.Clear();
            textBoxReceived.Clear();

        }



// ----------------------------------------------------------------------------
// ----------------------------------------------------------------------------

// X10 and comm
// Firecracker CM17A
// Active Home CM11A (not tested)

        /*
        Windows XP
        USB to serial comm port
        Prolific
        7/25/2005
        ver 2.0.2.1

        also works in Windows 7
        */



        private void openComm()
        {

            if (m_comm.IsOpen)
                return;

            DebugPrint("Open", true); // true = textBox1.refresh

            try
            {
                m_comm.PortName = "COM" + textComPort.Text; // color red 255, 128, 128, green 128, 255, 128

                if (m_comm.IsOpen == false)
                {
                    m_comm.Open();

                    // The first set of commands alway does not take affect, so we're sending this dummy command bellow

                    SendCommand("1111111111111111");

                    //                    //Send Header
                    //                    SendBits("1101010110101010");
                    //                    //Send Data
                    //                    SendBits("1111111111111111");
                    ////                    SendBits("1000000001100000"); // from table: E5 OFF          10000000   01100000
                    //                    //Send Footer
                    //                    SendBits("10101101");        


                }

                //              DebugPrint("Openned");

                textComPort.BackColor = Color.LightBlue;
                
                Color g = Color.LightGreen;
//              Color red = Color(1, 1, 1, 1);
                //Color red = 
                if (m_comm.IsOpen == true)
                    textComPort.BackColor = g; //(128, 255, 128, 255);



                // using Microsoft.Win32;
                // VB6 SaveSetting
          //      SaveSetting("textComPort", textComPort.Text);
                //Registry.SetValue(m_reg_current_user, "textComPort", textComPort.Text);

            }
            catch (Exception e1)
            {

                DebugPrint(e1.Message);
            }


        }



        private void SendBits(string data)
        {
            int i;

            for (i = 0; i < data.Length; i++)
            {
                if (data[i] == '1') //char
                {
                    m_comm.RtsEnable = true;
                    m_comm.DtrEnable = false;
                }
                else
                {
                    m_comm.RtsEnable = false;
                    m_comm.DtrEnable = true;
                }

                // pause in milliseconds
                // System.Threading.Thread.Sleep(1);

                m_comm.RtsEnable = true;
                m_comm.DtrEnable = true;

            }//for

        }//SendBits()



        private string m_cmd;
        private System.Threading.Thread m_thread;

        private void SendCommand(string cmd)
        {


            m_cmd = cmd;

            //System.Threading.Thread T1 = new System.Threading.Thread(SendCommandThread);
            //T1.Start();
            //T1.Join();

            m_thread = new System.Threading.Thread(SendCommandThread);
            m_thread.Start();
            //            m_thread.Join();




            ////Send Header
            //SendBits("1101010110101010");
            ////Send Data
            //SendBits(m_cmd);
            ////Send Footer
            //SendBits("10101101");

        }

        private void SendCommandThread()
        {
            //Send Header
            SendBits("1101010110101010");
            //Send Data
            SendBits(m_cmd);
            //Send Footer
            SendBits("10101101");
        }




        private void DebugPrint(string s)
        {
            DebugPrint(s,true);
        }        

        private void DebugPrint(string s, bool refresh)
        {
        }

        private void btnOpenComm_Click(object sender, EventArgs e)
        {
            openComm();
        }

        private void btnCloseComm_Click(object sender, EventArgs e)
        {
            m_comm.Close();
            DebugPrint("Close");
            textComPort.BackColor = Color.LightPink; //there's no LightRed :(
        }

        private void buttonON1_Click(object sender, EventArgs e)
        {
            //MessageBox.Show("ON1");
            lblLightBulb1.BackColor = Color.Yellow;
        }

        private void buttonOFF1_Click(object sender, EventArgs e)
        {
            lblLightBulb1.BackColor = Color.Silver;
        }

        private void buttonON2_Click(object sender, EventArgs e)
        {
             lblLightBulb2.BackColor = Color.Yellow;
        }

        private void buttonOFF2_Click(object sender, EventArgs e)
        {
            lblLightBulb2.BackColor = Color.Silver;
        }

        private void buttonON3_Click(object sender, EventArgs e)
        {

        }

        private void buttonOFF3_Click(object sender, EventArgs e)
        {

        }

        private void buttonON4_Click(object sender, EventArgs e)
        {

        }

        private void buttonOFF4_Click(object sender, EventArgs e)
        {

        }

        private void buttonON5_Click(object sender, EventArgs e)
        {

        }

        private void buttonOFF5_Click(object sender, EventArgs e)
        {
            string t = "OFF5 - " + textBoxHouse5.Text + ":" + textBoxModule5.Text;

            MessageBox.Show(t);

        }

// ----------------------------------------------------------------------------


    } // class

} // namespace
